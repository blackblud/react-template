import React from 'react';
import Home from '../pages/Home';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

export default function Index() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          <Route path="/login" element={<Home />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
}
